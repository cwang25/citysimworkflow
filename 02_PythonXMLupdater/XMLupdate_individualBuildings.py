import xml.etree.ElementTree as ET
import psycopg2
import pandas as pd

###### Set up XML file path here ######
tree = ET.parse('input/individualBuilding_XML_EGIDupdated.xml')
xmlRoot = tree.getroot()


def individualBuildingQuery(EGID):
    ###### Execute DB SQL query #####
    conn = psycopg2.connect("host=localhost dbname=CitySimData user=postgres password=ia09")
    cur = conn.cursor()

    # Retrieve data from DB
    cur.execute(
        """
        select * from public."Statistic" 
        where "EGID" =""" + str(EGID) + """;
        """
    )
    # table content
    data = cur.fetchall()
    # table schema
    colnames = [desc[0] for desc in cur.description]
    # convert into pd.DataFrame
    retrieved = pd.DataFrame(data, columns=colnames)
    return retrieved


def joinBlockedQuery(BuildingID):
    ###### Execute DB SQL query #####
    conn = psycopg2.connect("host=localhost dbname=CitySimData user=postgres password=ia09")
    cur = conn.cursor()

    # Retrieve data from DB
    cur.execute(
        """
        select * from public."Statistic" 
        where "EGID" =""" + str(buildingID) + """;
        """
    )
    # table content
    data = cur.fetchall()
    # table schema
    colnames = [desc[0] for desc in cur.description]
    # convert into pd.DataFrame
    retrieved = pd.DataFrame(data, columns=colnames)
    return retrieved

def xmlUpdate(ID, xmlRoot):

    class building:
        def __init__(self, occupants, heatSource, uValue):
            self.occupants = str(occupants)
            self.heatSource = str(heatSource)
            self.uValue = str(uValue)

    # execute SQL query with the given building ID. DB values used to update XML file
    retrieved = individualBuildingQuery(ID)
    dbOccupants = retrieved.iloc[0]['PersTotal']
    dbHeatSource = retrieved.iloc[0]['EnergieN']
    dbUvalue = 10
    bAttribute = building(dbOccupants, dbHeatSource, dbUvalue)

    # Updating building attributes in XML file
    for building in xmlRoot.iter('Building'):
        xmlEGID = int(building.get('key'))
        if xmlEGID == ID:
            for zone in building.iter('Zone'):
                zoneID = zone.get('id')
                for occupancy in zone.iter('Occupants'):
                    occupancy.set('n', bAttribute.occupants)
                for wall in zone.iter('Wall'):
                    wall.set('type', bAttribute.uValue)
                for floor in zone.iter('Floor'):
                    floor.set('type', bAttribute.uValue)
                for roof in zone.iter('Roof'):
                    roof.set('type', bAttribute.uValue)

    tree.write('output/individualBuilding_XML_Updated.xml')


def updateAll(xmlRoot):
    for building in xmlRoot.iter('Building'):
        xmlEGID = int(building.get('key'))
        xmlUpdate(xmlEGID, xmlRoot)

updateAll(xmlRoot)
