##############
# 1. For convenience, this script will run all importing scripts in this folder,
#    import the data to the corresponding DB tables
# 2. Before executing, please make sure the file paths, database setting are correct
# 3. If you specify and use the file path and connection setting here, it will overwrite the default path and
#    setting in each script, otherwise, it use the default setting inside each script
# 4. If encounter any encoding problem, open the data with notepad, change the encoding to "UTF-8" and save as csv.
#
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 15.02.2018
##############

import importBuildingTypes
import importMap
import importStatistics
import psycopg2
import os

# Specify connection setting and file path here
cwd = os.getcwd()  # current working directory
filePath_bTypes = cwd + '/../00_Geometry_DBdata/DBdata/buildingTypes.csv'
filePath_map = cwd + '/../00_Geometry_DBdata/DBdata/171113_EGID to BuildingID_building blocks average height.csv'
filePath_Statistic = cwd + '/../00_Geometry_DBdata/DBdata/Statistic.csv'
# Specify database connection setting here
conn = psycopg2.connect("host=localhost dbname=scenario01 user=postgres password=ia09")

# Leave the function arguments empty will execute default setting
def main():
    importBuildingTypes.main(filePath_bTypes, conn)
    importMap.main(filePath_map, conn)
    importStatistics.main(filePath_Statistic, conn)

if __name__ == "__main__":
    main()