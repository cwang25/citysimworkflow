1. Two ways to import data
    a. import individually by running each script
    b. importAll will automatically import all data into DB
    Before executing, be sure the file path and database setting are correct
2. Due to permission issue, using "INSERT" query instead of commonly used "COPY"
4. If encounter any encoding problem, open the data with notepad, change the encoding to "UTF-8" and save as csv.