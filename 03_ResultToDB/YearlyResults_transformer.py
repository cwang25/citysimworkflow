##############
# This script will parse CitySim simulation output: YearlyResultsPerBuilding and convert it into manageable data model and save as csv file.
# It also automatically updates the corresponding DB table. To start, first specify "filePath", "outputPath", "connection below", and "xmlPath".
# If automatically update to DB table is not needed, simply comment out the function updateDBData() in run()
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 07.02.2018
##############

import pandas as pd
import psycopg2
import os
import csv

cwd = os.getcwd()  # current working directory

# the function transposes and transforms the Area output into appropriate data model for database
def parseTable(filePath):
    content = pd.read_csv(filePath, delim_whitespace=True)
    [numRow, numCol] = content.shape
    content.insert(0, 'BuildingID', 1)
    content.columns = ['id','BuildingID', 'heatingNeeds_Wh', 'coolingNeeds_Wh']

    for i in range(numRow):
        idRow = content.iloc[i, 1]
        idStartP = idRow.find('(') + 1
        idEndP = idRow.find(')')
        ID = idRow[idStartP:idEndP]
        content.iloc[i, 1] = ID

    return content.iloc[:, 1:]

def updateDBData(outputPath, conn):
    cur = conn.cursor()
    # Retrieve data from DB, this table is joined by three tables
    cur.execute(
        """
    DROP TABLE IF EXISTS public."YearlyResults";

    CREATE TABLE public."YearlyResults"
    (
        "BuildingID" integer,
        "heatingNeeds_Wh" double precision,
        "coolingNeeds_Wh" double precision
    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public."YearlyResults"
        OWNER to postgres;
        """
    )

    with open(outputPath, 'r') as Output:
        reader = csv.reader(Output)
        next(reader)
        for row in reader:
            cur.execute(
                """INSERT INTO public."YearlyResults" VALUES (%s, %s, %s)""",
                row
            )

    cur.close()
    conn.commit()

# If run individual script with individual connection and file path setting, specify inside the function.
# Otherwise, it will run with the given path and connection setting specified in runAll()
def main(inPath = None, oPath = None, connPsql = None):
    if inPath and oPath and connPsql is not None:
        filePath = inPath
        outputPath = oPath
        conn = connPsql
    else:
        # default file path and database connection setting
        filePath = cwd + '/CitySimResult/baseline/Baseline_updatedXML_YearlyResultsPerBuilding.OUT'
        outputPath = cwd + '/CitySimResult/baseline/cleanedTables/cleanedYearlyResults.csv'
        conn = psycopg2.connect("host=localhost dbname=baseline user=postgres password=ia09")

    result = parseTable(filePath)
    # write the final cleaned table into csv
    result.to_csv(outputPath, index=False)
    print "Cleaned table is now available at " + str(outputPath)
    # automatically update the latest result to DB table
    updateDBData(outputPath, conn)

if __name__ == "__main__":
    main()