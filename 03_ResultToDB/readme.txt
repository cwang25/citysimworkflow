1. Two ways to rearrange simulation results and export them as csv. or into DB
    a. export individually by running each script
    b. runAll() will automatically export all outputs into csv. and DB
    Before executing, be sure the file path and database setting are correct
2. In case (b), you only need to specify DB conn, folderName, input file name and
    xml path
