##############
# This script will parse CitySim simulation output: Area and convert it into manageable data model and save as csv file
# and automatically update the corresponding DB table. To start, first specify "filePath", "outputPath", "connection below", and "xmlPath".
# If automatically update to DB table is not needed, simply comment out the function updateDBData() in run()
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 07.02.2018
##############

import pandas as pd
import xml.etree.ElementTree as ET
import psycopg2
import os
import csv

cwd = os.getcwd()  # current working directory

# the function transposes and transforms the Area output into appropriate data model for database
def parseTable(filePath, xmlRoot):
    content = pd.read_csv(filePath, delim_whitespace=True)
    content = content.transpose()
    [numRow, numCol] = content.shape
    # Adding two extra columns in the original table
    content.insert(0, 'BuildingID', 1)
    content.insert(5, 'Volume_m3', 1)
    content.columns = ['BuildingID', 'Floor_m2', 'Roof_m2', 'Wall_m2', 'Windows_m2', 'Volume_m3']

    for i in range(numRow):
        idRow = content.index[i]
        idStartP = idRow.find('(') + 1
        idEndP = idRow.find(')')
        ID = idRow[idStartP:idEndP]
        # if ID is not value, can not perform type conversion
        if ID.isalpha():
            continue
        else:
            zoneVolume = getZoneVolume(int(ID), xmlRoot)
        content.iloc[i, 0] = ID
        content.iloc[i, 5] = zoneVolume

    return content.iloc[1:, :]

# retrieve zone volume and add into Area.csv and Database
def getZoneVolume(ID, xmlRoot):
    zoneVolume = 'null'
    for building in xmlRoot.iter('Building'):
        buildingID = int(building.get('key'))
        if buildingID == ID:
            for zone in building.iter('Zone'):
                zoneVolume = zone.get('volume')
    return zoneVolume


def updateDBData(outputPath, conn):
    cur = conn.cursor()
    # Retrieve data from DB, this table is joined by three tables
    cur.execute(
        """
    DROP TABLE IF EXISTS public."Area";

    CREATE TABLE public."Area"
    (
        "BuildingID" integer,
        "Floor_m2" double precision,
        "Roof_m2" double precision,
        "Wall_m2" double precision,
        "Windows_m2" double precision,
        "Volume_m3" double precision
    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

    ALTER TABLE public."Area"
        OWNER to postgres;
        """
    )

    with open(outputPath, 'r') as Output:
        reader = csv.reader(Output)
        next(reader)
        for row in reader:
            cur.execute(
                """INSERT INTO public."Area" VALUES (%s, %s, %s, %s, %s, %s)""",
                row
            )

    cur.close()
    conn.commit()

# If run individual script with individual connection and file path setting, specify inside the function.
# Otherwise, it will run with the given path and connection setting specified in runAll()
def main(inPath = None, oPath = None, xmlPath = None, connPsql = None):
    if inPath and oPath and connPsql is not None:
        filePath = inPath
        outputPath = oPath
        tree = xmlPath
        conn = connPsql
    else:
        # default file path and database connection setting
        filePath = cwd + '/CitySimResult/baseline/Baseline_updatedXML_Area.out'
        outputPath = cwd + '/CitySimResult/baseline/cleanedTables/cleanedArea.csv'
        # default XML file path, this is used to get zone volume
        tree = ET.parse(cwd + '/CitySimResult/baseline/Baseline_updatedXML.xml')
        conn = psycopg2.connect("host=localhost dbname=baseline user=postgres password=ia09")

    xmlRoot = tree.getroot()
    result = parseTable(filePath, xmlRoot)
    # write the final cleaned table into csv
    result.to_csv(outputPath, index=False)
    print "Cleaned table is now available at " + str(outputPath)
    # automatically update the latest result to DB table
    updateDBData(outputPath, conn)

if __name__ == "__main__":
    main()