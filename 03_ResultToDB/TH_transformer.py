##############
# This script will parse CitySim simulation output: TH and convert it into manageable data model and save as csv file
# and automatically update the corresponding DB table. To start, first specify filePath, outputPath and connection below.
# If automatically update to DB table is not needed, simply comment out the function updateDBData() in run()
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 07.02.2018
##############

import pandas as pd
import psycopg2
import os
import csv

cwd = os.getcwd()  # current working directory
# schemaMap is used to look up the corresponding attributes in the simulation output
# and rearrange into database friendly data model
schemaMap = cwd + '/TH_schemaMap.csv'


# transpose the table and parse header into BuildingID and data type
# this function is just easy for later operation
def parseTable(filePath, schema):
    content = pd.read_csv(filePath, delim_whitespace=True)
    content = content.transpose()
    [numRow, numCol] = content.shape
    [numMapRow, numMapCol] = schema.shape

    # add two new columns, the parsed BuildingID and data type will append to these two columns
    content.insert(0, 'BuildingID', 1)
    content.insert(1, 'dataType', 1)

    for i in range(numMapRow):
        rawHeader = schema.iloc[i, 0]
        schemaHeader = schema.iloc[i, 1]
        for j in range(numRow):
            # This part is used to parse BuildingID from the result table
            header = str(content.index[j])
            idStartP = header.find('(') + 1
            idEndP = header.find(')')
            BuildingID = header[idStartP:idEndP]
            if rawHeader in header:
                content.iloc[j, 1] = schemaHeader
                content.iloc[j, 0] = BuildingID
    return content.iloc[1:, :]

# reshape the table structure so it can be easily dump into DB and for later analysis and management
def reshapeTable(content):
    groupedTable = content.groupby(content.iloc[:, 0])
    Keys = groupedTable.groups.keys()

    column = ['BuildingID', 'timeStep', 'Ta_celsius', 'Heating_Wh', 'Cooling_Wh', 'Qi_Wh', 'Qs_Wh', 'VdotVent_m3_h',
              'HeatStockTemp_celsius', 'ColdStockTemperature_celsius', 'MachinePower_W', 'FuelConsumption_MJ',
              'ElectricConsumption_kWh', 'SolarThermalProduction_Wh']

    reshapedTable = []
    for i in range(len(Keys)):
        singleBuilding = groupedTable.get_group(Keys[i]).transpose()[2:]
        [rows, cols] = singleBuilding.shape
        for j in range(rows):
            record = [str(Keys[i]), j + 1]
            record.extend(singleBuilding.iloc[j, :])
            reshapedTable.append(tuple(record))

    return pd.DataFrame(reshapedTable, columns=column)


# this function is not completed yet
def updateDBData(outputPath, conn):
    ###### Execute DB SQL query #####
    cur = conn.cursor()

    # Retrieve data from DB, this table is joined by three tables
    cur.execute(
        """
        DROP TABLE IF EXISTS public."TH_Building";

        CREATE TABLE public."TH_Building"
        (
            "BuildingID" integer,
            "timeStep" integer,
            "Ta_celsius" double precision,
            "Heating_Wh" double precision,
            "Cooling_Wh" double precision,
            "Qi_Wh" double precision,
            "Qs_Wh" double precision,
            "VdotVent_m3_h" double precision,
            "HeatStockTemp_celsius" double precision,
            "ColdStockTemp_celsius" double precision,
            "MachinePower_W" double precision,
            "FuelConsumption_MJ" double precision,
            "ElectricConsumption_kWh" double precision,
            "SolarThermalProduction_Wh" double precision
        )
        WITH (
            OIDS = FALSE
        )
        TABLESPACE pg_default;
        
        ALTER TABLE public."TH_Building"
            OWNER to postgres;
                 
        """
    )

    with open(outputPath, 'r') as Output:
        reader = csv.reader(Output)
        next(reader)
        for row in reader:
            cur.execute(
                """INSERT INTO public."TH_Building" VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                row
            )

    cur.close()
    conn.commit()

# If run individual script with individual connection and file path setting, specify inside the function.
# Otherwise, it will run with the given path and connection setting specified in runAll()
def main(inPath = None, oPath = None, connPsql = None):
    if inPath and oPath and connPsql is not None:
        filePath = inPath
        outputPath = oPath
        conn = connPsql
    else:
        # default file path and database connection setting
        filePath = cwd + '/CitySimResult/baseline/Baseline_updatedXML_TH.out'
        outputPath = cwd + '/CitySimResult/baseline/cleanedTables/cleanedTH.csv'
        conn = psycopg2.connect("host=localhost dbname=baseline user=postgres password=ia09")

    schema = pd.read_csv(schemaMap, sep=';')
    content = parseTable(filePath, schema)
    result = reshapeTable(content)
    # write the final cleaned table into csv
    result.to_csv(outputPath, index=False)
    print "Cleaned table is now available at " + str(outputPath)
    # automatically update the latest result to DB table
    updateDBData(outputPath, conn)

if __name__ == "__main__":
    main()
