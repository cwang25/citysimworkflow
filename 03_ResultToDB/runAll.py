##############
# For convenience, this script will run all transformers in this folder,
# create cleaned simulation outputs and update the corresponding DB tables
# Before executing, please make sure the file paths are correct
# In most cases, you only need to specify DB conn, folderName and input file name
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 07.02.2018
##############

import Area_transformer
import TH_transformer
import YearlyResults_transformer
import psycopg2
import os
import xml.etree.ElementTree as ET

cwd = os.getcwd()  # current working directory

###### Set up DB connection here ######
conn = psycopg2.connect("host=localhost dbname=scenario01 user=postgres password=ia09")

# change folderName here to distribute the results to different folders
folderName = '/scenario01'
path = cwd + '/CitySimResult' + folderName
if not os.path.exists(path):
    os.makedirs(path)
outputPath = path + '/cleanedTables/'

###### Set up input file path here ######
inPath_Area = path + '/Baseline_updatedXML_Area.out'
inPath_TH = path + '/Baseline_updatedXML_TH.out'
inPath_YearlyResults = path + '/Baseline_updatedXML_YearlyResultsPerBuilding.out'
tree = ET.parse(path + '/Baseline_updatedXML.xml')

###### Set up output csv file path here ######
# In most cases, no need to modify here
outPath_Area = outputPath + 'cleanedArea.csv'
outPath_TH = outputPath + 'cleanedTH.csv'
outPath_YearlyResults = outputPath + 'cleanedYearlyResults.csv'


def main():
    Area_transformer.main(inPath_Area, outPath_Area, tree, conn)
    TH_transformer.main(inPath_TH, outPath_TH, conn)
    YearlyResults_transformer.main(inPath_YearlyResults, outPath_YearlyResults, conn)

if __name__ == "__main__":
    main()



