Slingshot!  
Database Plug-In for Grasshopper
version 0.8.8.0

Copyright (c) 2012 Nathan Miller
Website:  http://theprovingground.wikidot.com/slingshot
Blog:  http://nmillerarch.blogspot.com
E-Mail:  nmiller.arch@gmail.com


---------------------------------------------------------------------------------

Requirements

You must have the following installed on your computer�

1. Rhinoceros 3D *
2. Grasshopper 3D 0.8.0066
3. ODBC, OLE DB, or MySQL Database **

* Some features (Serialization) are only supported in Rhino 5.
** Tested with MySQL, PostgrSQL, Excel, and SQLite

VERSION HISTORY:  http://theprovingground.wikidot.com/slingshot-db-version

---------------------------------------------------------------------------------

INSTALLATION INSTRUCTIONS

1. Close all running instances of Rhino and Grasshopper
2. Place the SlingshotDB.gha MySQL.Data.dll, SQLite.Interop.dll, and System.Data.Sqlite.dll into your Grasshopper Components folder.
3. Launch Rhino and Grasshopper
4. A tab titled "Slingshot!" should now appear as a Grasshopper tab

By downloading and installing the Slingshot! Database plug-in, you agree to 
the following license.

---------------------------------------------------------------------------------

LICENSE

TERMS AND CONDITIONS
�This License� refers to the version of this public license, in the form it 
is accompanying this Program. �The Program� refers to any copyrightable work 
licensed under this license. Each licensee will be addressed as �you�. 
�Recipients� and �Licensees� may be individuals or organizations. �Access�
means to use or benefit from using the functionality of the Program.

ALL RIGHTS RESERVED
Except as expressly provided otherwise in this agreement - title, ownership 
and all rights and interest including, without limitation, copyrights, patents, 
trademarks, trade secrets and other intellectual property rights, in and to 
the Licensor and any authorized copies made by you remain with the licensor. 
The content, the form, and code of this product are valuable trade secrets of 
the Licensor and you shall keep such trade secrets confidential. This Program 
is licensed, not given away for free.

PERMITTED ACTIONS
With respect to any version of the Program, you may convey unlimited verbatim 
backup copies of the Program for backup purposes in the event that your primary 
copy of the Program becomes inoperable. You may install unlimited copies of the 
Program for your usage purposes.

PROHIBITED ACTIONS
The licensor does not allow any of the following actions and you acknowledge 
that such actions shall be prohibited. You may not, and may not permit any 
third party to, reverse engineer, modify or disassemble the Program. You may 
not rent, loan, lease, sell, sublicense, or otherwise provide access to any 
portion of the Program, or any rights granted in this Agreement, to any other 
person without the prior written consent of the licensor. You may not remove, 
alter, or obscure any proprietary names or notices, labels, or marks from the 
Program, indistinctively of their origin and belonging.

END OF LICENSE
You may terminate this License by removing all copies of the Program from your 
possession.  Without prejudice to any other rights, the Licensor may terminate 
the License if you fail to comply with the terms and conditions of the License.  
In such event, all copies of the program in your possession must be destroyed.

SURRENDER OF OTHER PEOPLE'S FREEDOM
If conditions are imposed on you (whether by court order, agreement or otherwise) 
that contradict the conditions of this license, they do not excuse you from the 
previously agreed conditions of this license.

DISCLAIMER OF WARRANTY
This Program is provided to you "AS IS", with no express or implied warranty, 
not even merchantability, fitness for a particular purpose and non-infringement. 
Without limiting the foregoing, the licensor does not warrant that the operation 
of the software will be uninterrupted or error free.

LIMITATION OF LIABILITY
In no event unless required by applicable law shall the Licensor have any 
liability for any incidental, special, indirect, or consequential damages, 
loss of profits, revenue, data, or cost of cover. In addition, in no event 
shall the liability of the licensor for any damages arising out of or in 
connection with the Program, user documentation or this agreement exceed 
the amount paid or payable by you for the Program directly responsible for 
such damages. The limitations of liability in this section shall apply to 
any damages, however caused and regardless of the theory of liability, 
whether derived from contract, tort (including, but not limited to, negligence), 
or otherwise, even if the licensor has been advised of the possibility of 
such damages and regardless of whether the limited remedies available 
hereunder fail of their essential purpose. The Licensor shall have no 
responsibility or liability whatsoever arising from loss or theft of the 
Program or the media on which the Program is furnished to you. You can choose 
if the Program, after the first run, will connect to look for updates for 
the current release. This service might not work, among other reasons, 
because of Internet connection unavailability. The Licensor shall not be 
obligated to replace any lost or stolen Program or Program media. You are 
solely responsible for safeguarding the Program and the media on which the 
Program is furnished.
